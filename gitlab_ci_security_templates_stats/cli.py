#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import click
import gitlab_ci_security_templates_stats.analyze_repos as analyze_repos
import gitlab_ci_security_templates_stats.clone_repos as clone_repos
import gitlab_ci_security_templates_stats.data_management as data_management
import gitlab_ci_security_templates_stats.generate_repo_list as generate_repo_list


@click.group()
@click.option(
    "-d",
    "--debug",
    is_flag=True,
    default=False,
    show_default=True,
    help="Show some debug information.",
)
@click.option(
    "-v",
    "--verbose",
    is_flag=True,
    default=False,
    show_default=True,
    help="Show additional repo-level data.",
)
@click.pass_context
def cli(ctx, debug, verbose):
    ctx.ensure_object(dict)
    ctx.obj["debug"] = debug
    ctx.obj["verbose"] = verbose


@cli.command("help")
def help():
    with click.Context(cli) as ctx:
        click.echo(cli.get_help(ctx))


@cli.command("repos")
@click.pass_context
def repos(ctx):
    """Locally cache a curated list of Gitlab repos."""
    generate_repo_list.check_local_cache_dir()
    generate_repo_list.locally_cache_repo_list()
    clone_repos.clone_all_repos()


@cli.command("stats")
@click.pass_context
def stats(ctx):
    """Generate full AppSec include usage stats."""
    repo_data = []
    repo_data = analyze_repos.walk_and_analyze_repos()
    data_management.output_report_results(repo_data, ctx.obj["verbose"])


if __name__ == "__main__":
    cli()
