#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import git
import glob
import pathlib
import shutil
import uuid
import validators
import gitlab_ci_security_templates_stats.config as config


def attempt_local_clone(project_name, project_repo_url):
    """attempt to clone local copy of repo"""
    cfg = config.get_config()
    return_val = True
    project_local_repo_path = (
        cfg["app.cr.local_repo_dir"] + "/" + project_name + "-" + str(uuid.uuid4())[:8]
    )
    valid_url = validators.url(project_repo_url)

    if valid_url is True:
        try:
            git.Repo.clone_from(
                project_repo_url, project_local_repo_path, depth=1
            )  # assumes public repo, shallow clone
        except git.exc.GitError as err:
            return_val = str(err)
    else:
        raise RuntimeError(
            f"Invalid git project url was provided: {project_repo_url}."
        )  # noqa E501

    return return_val


def clone_all_repos():
    cfg = config.get_config()

    """clear local repos dir"""
    subdirs_pathname = cfg["app.cr.local_repo_dir"] + "/*/"
    subdirs_list = glob.glob(subdirs_pathname)
    for subdir in subdirs_list:
        shutil.rmtree(subdir)

    with open(cfg["app.grl.data_file_path"]) as file:
        while line := file.readline():
            git_url = line.rstrip()
            git_url_path = pathlib.PurePath(git_url)
            git_project_name = git_url_path.name.split(".git")[0]

            return_val = attempt_local_clone(git_project_name, git_url)
            if return_val != True:  # noqa: E712
                print(return_val)
    print(
        "Successfully cloned all repos from "
        + cfg["app.grl.data_file_path"]
        + " locally."
    )
