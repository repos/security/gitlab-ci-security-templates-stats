#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import texttable
import gitlab_ci_security_templates_stats.config as config


def _nice_pcnt_str(val):
    return " (" + str(round((val) * 100, 2)) + "%)"


def output_report_results(results, verbose_opt=False):
    cfg = config.get_config()

    tot_repo_cnf = int(sum(1 for _ in open(cfg["app.grl.data_file_path"], "rbU")))
    tot_repo_cln = int(len(os.listdir(cfg["app.cr.local_repo_dir"])))
    tot_repo_no_ci = int(
        sum(1 for k, v in results.items() if v == "No " + cfg["app.ar.gitlab_ci_file"])
    )
    tot_repo_no_sec_inc = int(
        sum(1 for k, v in results.items() if isinstance(v, list) and len(v) < 1)
    )
    tot_repo_sec_inc = int(
        sum(1 for k, v in results.items() if isinstance(v, list) and len(v) > 0)
    )

    data = [
        ["Total Repos Configured (for this tool)", tot_repo_cnf],
        [
            "Total Repos Cloned (by this tool)",
            str(tot_repo_cln) + _nice_pcnt_str(tot_repo_cln / tot_repo_cnf),
        ],
        [
            "Repos With No " + cfg["app.ar.gitlab_ci_file"],
            str(tot_repo_no_ci) + _nice_pcnt_str(tot_repo_no_ci / tot_repo_cln),
        ],
        [
            "Repos With No Security Includes",
            str(tot_repo_no_sec_inc)
            + _nice_pcnt_str(tot_repo_no_sec_inc / tot_repo_cln),
        ],
        [
            "Repos With Security Includes",
            str(tot_repo_sec_inc) + _nice_pcnt_str(tot_repo_sec_inc / tot_repo_cln),
        ],
    ]

    table = texttable.Texttable()
    table.set_cols_align(["l", "r"])
    table.set_cols_dtype(["t", "t"])
    table.add_rows(data, header=False)

    print(table.draw())

    if verbose_opt:
        print("")
        print("*" * 80)
        print("")
        print("*** Raw Data ***")
        print("")
        for repo_name, includes in results.items():
            if isinstance(includes, list) and len(includes) > 0:
                print(repo_name.rsplit("-", 1)[0], includes, "\n")
