#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import git
import glob
import os
import pathlib
import re
import traceback
import yaml
import gitlab_ci_security_templates_stats.config as config


def walk_and_analyze_repos():
    cfg = config.get_config()
    repo_data = {}

    for subdir in glob.glob(cfg["app.cr.local_repo_dir"] + "/*/"):
        try:
            checkout_default_branch(subdir)
        except Exception as error:
            print(
                "An error occurred while performing a git checkout in repo {}: {}".format(  # noqa: E501
                    subdir, str(error)
                )
            )

        try:
            ci_file_path = subdir + cfg["app.ar.gitlab_ci_file"]
            if os.path.isfile(ci_file_path):
                found_includes = find_relevant_security_includes(
                    ci_file_path, find_and_parse_gitlab_ci_yml(ci_file_path)
                )
                if isinstance(found_includes, dict) and found_includes is not False:
                    repo_data.update(found_includes)
            else:
                """repo does not contain a .gitlab-ci.yml"""
                repo_data[pathlib.Path(subdir).name] = (
                    "No " + cfg["app.ar.gitlab_ci_file"]
                )
        except Exception:
            print(
                "An error occurred while reading and parsing {}: {}".format(
                    ci_file_path, traceback.format_exc()
                )
            )

    return repo_data


def checkout_default_branch(dir_path):
    """TODO: this is mostly a no-op for now, but should eventually support
    multiple branch checkouts and analysis"""
    if dir_path in [".", ".."]:
        return

    repo = None
    default_branch = None
    repo = git.Repo(dir_path)
    branch_info_show = repo.git.remote("show", "origin")
    matched_branches = re.search(r"\s*HEAD branch:\s*(.*)", branch_info_show)
    if (
        matched_branches
        and matched_branches.group(1)
        and matched_branches.group(1) != "(unknown)"
    ):
        default_branch = matched_branches.group(1)
    if default_branch is not None:
        repo.git.checkout(default_branch)
    else:
        print(
            "Default git branch not found for: {}, no checkout performed.".format(
                dir_path
            )
        )


def _any_constructor(loader, tag_suffix, node):
    """helper for yaml.safe_load
    https://stackoverflow.com/a/52241794"""
    if isinstance(node, yaml.MappingNode):
        return loader.construct_mapping(node)
    if isinstance(node, yaml.SequenceNode):
        return loader.construct_sequence(node)
    return loader.construct_scalar(node)


def find_and_parse_gitlab_ci_yml(ci_file_path):
    """ensure .gitlab-ci.yml exists and load as str data"""
    file_data = []
    yaml.add_multi_constructor("", _any_constructor, Loader=yaml.SafeLoader)
    with open(ci_file_path, "r") as file:
        file_data = yaml.safe_load(file)
    return file_data


def _rec_find_security_includes(yaml_fragment, cfg, repo_data, ci_file_path, keys=True):
    if isinstance(yaml_fragment, dict) or isinstance(yaml_fragment, list):
        if keys is False:
            return repo_data
        elif isinstance(keys, list):
            yaml_keys = keys
        else:
            for k, v in cfg["app.ar.yaml_keys"].items():
                if v == "0":
                    yaml_keys = [k]

        base_http_pat = "^https:\/\/"  # noqa: W605
        base_gitlab_url_pat = "^" + re.escape(cfg["app.grl.base_gitlab_url"]) + ".+"
        base_inc_pats = [re.escape(s) for s in cfg["app.ar.include_patterns"]]

        for key in yaml_keys:
            if isinstance(yaml_fragment, dict) and key in yaml_fragment.keys():
                if isinstance(yaml_fragment[key], str):
                    append_str = yaml_fragment[key]
                    if (
                        key == "project"
                    ):  # TODO: slightly hacky, should be more programmatic
                        for k, v in cfg["app.ar.yaml_keys"].items():
                            if v == "project" and k in yaml_fragment.keys():
                                append_str += " / " + yaml_fragment[k]
                    for pat in base_inc_pats:
                        if re.search(pat, yaml_fragment[key]) and re.match(
                            base_http_pat, yaml_fragment[key]
                        ):
                            if re.match(base_gitlab_url_pat, yaml_fragment[key]):
                                repo_data[
                                    pathlib.Path(ci_file_path).parent.name
                                ].append(append_str)
                        elif re.match("^" + pat, yaml_fragment[key]):
                            repo_data[pathlib.Path(ci_file_path).parent.name].append(
                                append_str
                            )
                elif isinstance(yaml_fragment[key], list):
                    new_keys = []
                    for item in yaml_fragment[key]:
                        if isinstance(item, str):
                            append_str = item
                            for pat in base_inc_pats:
                                if re.search(pat, item) and re.match(
                                    base_http_pat, item
                                ):
                                    if re.match(base_gitlab_url_pat, item):
                                        repo_data[
                                            pathlib.Path(ci_file_path).parent.name
                                        ].append(append_str)
                                elif re.match("^" + pat, item):
                                    repo_data[
                                        pathlib.Path(ci_file_path).parent.name
                                    ].append(append_str)
                                elif item in cfg["app.ar.yaml_keys"]:
                                    new_keys.append(item)
                        elif isinstance(item, dict):
                            new_keys = list(item.keys())
                        if new_keys is not False:
                            _rec_find_security_includes(
                                item, cfg, repo_data, ci_file_path, new_keys
                            )
    else:
        return

    if isinstance(repo_data, dict) and len(repo_data) > 0:
        return repo_data


def find_relevant_security_includes(ci_file_path, file_data):
    """search for relevant security include patterns"""
    cfg = config.get_config()
    return_val = {}
    repo_data = {}
    repo_data[pathlib.Path(ci_file_path).parent.name] = []

    return_val = _rec_find_security_includes(file_data, cfg, repo_data, ci_file_path)

    return return_val
