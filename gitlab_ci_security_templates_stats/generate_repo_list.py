#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import re
import requests
import sys
from pathvalidate import sanitize_filepath
import gitlab_ci_security_templates_stats.config as config


def check_local_cache_dir():
    """exit with helpful message if local data dir has not been created"""
    cfg = config.get_config()
    base_dir = os.path.dirname(cfg["app.grl.data_file_path"])
    if not os.path.exists(base_dir):
        sys.exit(
            "The directory '"
            + base_dir
            + "' does not exist!  Please create it within this repository or choose another path within config.py."  # noqa: E501
        )


def get_group_ids_from_api(config):
    total_pages = 1
    per_page = 20
    group_ids = {}
    api_url = (
        config["app.grl.base_gitlab_url"]
        + config["app.grl.base_gitlab_api_path"]
        + "/groups"
    )

    resp = requests.get(api_url, timeout=16)
    if resp.status_code == 200:
        if "X-Total-Pages" in resp.headers:
            total_pages = int(resp.headers["X-Total-Pages"])
        if "X-Per-Page" in resp.headers:
            per_page = int(resp.headers["X-Per-Page"])
        for item in resp.json():
            for namespace_pat in config["app.grl.group_namespaces"]:
                if re.match("^" + re.escape(namespace_pat) + ".*", item["full_path"]):
                    group_ids[item["full_path"]] = item["id"]

        page_count = 2
        while page_count <= total_pages:
            sub_api_url = (
                api_url + "?page=" + str(page_count) + "&per_page=" + str(per_page)
            )
            sub_resp = requests.get(sub_api_url, timeout=16)
            if sub_resp.status_code == 200:
                for item in sub_resp.json():
                    for namespace_pat in config["app.grl.group_namespaces"]:
                        if re.match(
                            "^" + re.escape(namespace_pat) + ".*", item["full_path"]
                        ):
                            group_ids[item["full_path"]] = item["id"]
                page_count += 1
            else:
                raise ValueError("Gitlab API request returned non-200 response code!")
    else:
        raise ValueError("Gitlab API request returned non-200 response code!")

    return group_ids


def get_project_repo_urls_by_group_ids(config, group_ids):
    if not hasattr(group_ids, "__len__"):
        raise ValueError("group_ids should be a list/array!")

    project_repo_data = {}

    api_url = (
        config["app.grl.base_gitlab_url"]
        + config["app.grl.base_gitlab_api_path"]
        + "/groups"
    )
    for group_full_path, group_id in group_ids.items():
        sub_api_url = api_url + "/" + str(group_id) + "/projects"
        resp = requests.get(sub_api_url, timeout=16)
        if resp.status_code == 200:
            project_repo_data[group_full_path] = []
            for group_project in resp.json():
                if (
                    group_project["http_url_to_repo"]
                    not in config["app.grl.exclude_repos"]
                ):
                    project_repo_data[group_full_path].append(
                        group_project["http_url_to_repo"]
                    )
            if len(project_repo_data[group_full_path]) == 0:
                del project_repo_data[group_full_path]
        else:
            raise ValueError("Gitlab API request returned non-200 response code!")

    return project_repo_data


def write_group_repo_data_to_file(config, group_repo_data):
    if not hasattr(group_repo_data, "__len__"):
        raise ValueError("group_repo_data should be a list/array!")

    file_data_str = ""
    for group_repos in group_repo_data.values():
        if not hasattr(group_repos, "__len__"):
            raise ValueError("group_repos should be a list/array!")
        else:
            file_data_str += "\n".join(group_repos) + "\n"

    with open(sanitize_filepath(config["app.grl.data_file_path"]), "w") as datafile:
        datafile.write(file_data_str)


def locally_cache_repo_list():
    try:
        cfg = config.get_config()
        group_ids = get_group_ids_from_api(cfg)
        group_repo_data = get_project_repo_urls_by_group_ids(cfg, group_ids)
        write_group_repo_data_to_file(cfg, group_repo_data)
        print(
            "Repo list successfully created and cached within: "
            + cfg["app.grl.data_file_path"]
        )
    except Exception as error:
        print(
            "An error occurred while attempting to locally cache the repo list: {}".format(  # noqa: E501
                error
            )
        )
