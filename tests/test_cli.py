#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from click.testing import CliRunner
from gitlab_ci_security_templates_stats.cli import cli


def test_cli_help():
    runner = CliRunner()

    help_text_expected = """[OPTIONS] COMMAND [ARGS]...

Options:
  -d, --debug    Show some debug information.
  -v, --verbose  Show additional repo-level data.
  --help         Show this message and exit.

Commands:
  help
  repos  Locally cache a curated list of Gitlab repos.
  stats  Generate full AppSec include usage stats."""

    """test help message"""
    result = runner.invoke(cli, ["--help"])
    assert result.exit_code == 0
    assert help_text_expected in result.output
