# Gitlab CI Security Templates Stats

A simple cli tool that generates some basic coverage reports and statistics for usage of the Gitlab CI Security Templates across Wikimedia projects.

## Installing

1. ```$ git clone "https://gitlab.wikimedia.org/repos/security/gitlab-ci-security-templates-stats.git"```
2. ```$ poetry install```
3. Create a ```data``` directory within the cloned directory (or whatever path you'd like to use and set within config.py)

## Usage

1. Have a look at gitlab_ci_security_templates_stats/config.py and assure the values are to your liking 
2. Build Gitlab repository URL list and clone repos: ```poetry run gcsts repos``` 
3. Run statistics: ```poetry run gcsts stats```

### Dependencies

1. Python: see [pyproject.toml](pyproject.toml)
2. [Git](https://git-scm.com/)

### CLI Usage

```
# install dependencies
$ poetry install

# basic help
$ poetry run gcsts --help

# it is important to note that these results are risk _score_ and not raw values
$ poetry run gcsts

```

## TODO

1. Improve unit test/code coverage
2. Implement a docker-compose

## Support

File a bug within [Phabricator](https://phabricator.wikimedia.org/maniphest/) and tag [#security-team](https://phabricator.wikimedia.org/project/profile/1179/).

## Contributing

Merge requests are always welcome.  For bugs and feature requests, please see support process noted above.  This project is governed by the [MediaWiki Code of Conduct](https://www.mediawiki.org/wiki/Code_of_Conduct).

## Authors

* **Scott Bassett** [sbassett@wikimedia.org]

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE](LICENSE) file for details.
